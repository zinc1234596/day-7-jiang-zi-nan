package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompaniesRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController()
@RequestMapping("/companies")
public class CompaniesController {
    @Resource
    private CompaniesRepository companiesRepository;
    @Resource
    private EmployeeRepository employeeRepository;

    @GetMapping()
    public List<Company> getAllCompanies() {
        return companiesRepository.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companiesRepository.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id) {
        return employeeRepository.getEmployeesByCompanyId(id);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return companiesRepository.addCompany(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companiesRepository.updateCompany(id, company);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable Long id) {
        companiesRepository.deleteCompanyById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesWithPagination(@RequestParam("page") Integer page, @RequestParam Integer size) {
        return companiesRepository.getCompaniesWithPagination(page, size);
    }
}
