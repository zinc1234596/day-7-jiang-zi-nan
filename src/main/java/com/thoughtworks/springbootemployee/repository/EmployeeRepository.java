package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();
    private static AtomicLong atomicId = new AtomicLong(0);

    public Employee addAnEmployee(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> getAllEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().get();
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employees.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id, Employee employee) {
        return employees.stream().filter(employeeItem -> employeeItem.getId().equals(id)).findFirst().map(item -> {
            item.setAge(employee.getAge());
            item.setSalary(employee.getSalary());
            return item;
        }).get();
    }

    public void deleteEmployeeById(Long id) {
        employees.removeIf(employee -> id.equals(employee.getId()));
    }

    public List<Employee> queryEmployeesWithPagination(Integer page, Integer size) {
        return employees.stream().skip((page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
