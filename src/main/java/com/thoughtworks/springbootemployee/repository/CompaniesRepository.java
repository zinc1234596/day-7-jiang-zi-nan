package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompaniesRepository {
    private List<Company> companies = new ArrayList<>();
    private static AtomicLong atomicId = new AtomicLong(0);

    public List<Company> getAllCompanies() {
        return companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream().filter(company -> company.getId().equals(id)).findFirst().get();
    }

    public Company addCompany(Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public Company updateCompany(Long id, Company company) {
        return companies.stream().filter(companyItem -> companyItem.getId().equals(id)).findFirst().map(item -> {
            item.setName(company.getName());
            return item;
        }).get();
    }

    public void deleteCompanyById(Long id) {
        companies.removeIf(employee -> id.equals(employee.getId()));
    }

    public List<Company> getCompaniesWithPagination(Integer page, Integer size) {
        return companies.stream().skip((page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
