### O:

- This morning, I summarized the knowledge of OO, TDD, and Refactor through conceptual diagrams, which allowed me to review the knowledge I learned last week.
- Next, I studied HTTP and Restful API. Since I have previously learned about HTTP and Restful API.
- In the afternoon, I mainly did some exercises on Spring Boot, which helped me gain a better understanding of web development by developing RESTful APIs using Spring Boot.

### R:

- I have a preliminary understanding of springboot
- Not comfortable with pair programming

### I:

- I am familiar with the HTTP and Restful API, but through further study, I noted some points that I didn't remember clearly before. For example, the detailed information about each component of REST and HTTP status codes like 301 and 304.

### D:

- I will keep my enthusiasm for learning